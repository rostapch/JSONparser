/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 19:11:25 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 19:11:27 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		json_write(t_value json, char *file)
{
	FILE *f;

	f = fopen(file, "w");
	if (f)
	{
		value_write(f, json, 0);
		fprintf(f, "\n");
		fclose(f);
	}
	else
		printf("File \"%s\" not found\n", file);
}

void		value_write(FILE *f, t_value json, int tabs)
{
	if (json.type == INT)
		write_int(f, json.value);
	else if (json.type == FLOAT)
		write_float(f, json.value);
	else if (json.type == STRING)
		write_string(f, json.value);
	else if (json.type == ARRAY)
		write_array(f, json.value, tabs);
	else if (json.type == OBJECT)
		write_obj(f, json.value, tabs);
}

void		write_obj(FILE *f, void *obj, int tabs)
{
	int		i;
	t_list	*current;
	t_list	*next;

	if (obj)
	{
		i = -1;
		fprintf(f, "{\n");
		current = ((t_object*)(obj))->pairs;
		while (current)
		{
			tabs++;
			next = current->next;
			write_pair(f, current->content, tabs);
			current = next;
			if (current)
				fprintf(f, ",\n");
			tabs--;
		}
		fprintf(f, "\n");
		while (++i < tabs)
			fprintf(f, "    ");
		fprintf(f, "}");
	}
}

void		write_array(FILE *f, void *arr, int tabs)
{
	t_list	*current;
	t_list	*next;

	if (arr)
	{
		fprintf(f, "[");
		current = ((t_array*)(arr))->values;
		while (current)
		{
			next = current->next;
			value_write(f, *(t_value*)(current->content), tabs);
			current = next;
			if (current)
				fprintf(f, ", ");
		}
		fprintf(f, "]");
	}
}
