/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   value.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:44:45 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:44:47 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_value		*parse_value(char **json, t_value *prnt)
{
	t_value		*value;

	if (!(**json && (value = (t_value*)ft_memalloc(sizeof(t_value)))))
		return (NULL);
	value->parent = prnt;
	if (**json == '{')
		value->value = parse_object(json, value);
	else if (**json == '\"')
	{
		value->type = STRING;
		value->value = parse_string(json);
	}
	else if (**json == '[')
		value->value = parse_array(json, value);
	else if (ft_isdigit(**json))
		value->value = parse_digit(json, value);
	else if (ft_isalpha(**json))
		value->value = parse_null(json);
	return (value);
}

void		free_value(t_value **val)
{
	(*val)->parent = NULL;
	if ((*val)->type == STRING)
		free_string((t_string**)&((*val)->value));
	else if ((*val)->type == INT || (*val)->type == FLOAT)
		ft_memdel((void **)&((*val)->value));
	else if ((*val)->type == ARRAY)
		free_array((t_array**)&((*val)->value));
	else if ((*val)->type == OBJECT)
		free_object((t_object**)&((*val)->value));
	ft_memdel((void **)&((*val)->value));
	ft_memdel((void **)&((*val)));
}
