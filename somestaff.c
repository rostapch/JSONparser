/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   somestaff.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:13:51 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:13:53 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		exit_error(int error)
{
	printf("Error while parsing json occured!\n");
	exit(error);
}

void		skip_spaces(char **json)
{
	while (**json == ' ' || **json == '\t' || **json == '\n')
		(*json)++;
}
