/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:47:58 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:48:01 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_array		*parse_array(char **json, t_value *prnt)
{
	t_array	*arr;

	prnt->type = ARRAY;
	if (!(**json) || (**json != '['))
		return (NULL);
	(*json)++;
	skip_spaces(json);
	if (!(arr = (t_array*)ft_memalloc(sizeof(t_array))))
		return (NULL);
	ft_lstqueueadd(&(arr->values), values_list(json, prnt));
	while (**json != ']')
	{
		skip_spaces(json);
		if (**json == ',')
		{
			(*json)++;
			ft_lstqueueadd(&(arr->values), values_list(json, prnt));
		}
	}
	(*json)++;
	return (arr);
}

t_list		*values_list(char **json, t_value *prnt)
{
	t_value		*value;
	t_list		*values;

	skip_spaces(json);
	if (**json == ',')
		(*json)++;
	skip_spaces(json);
	value = parse_value(json, prnt);
	values = ft_lstnew(value, sizeof(t_value));
	ft_memdel((void **)&value);
	return (values);
}

void		free_array(t_array **arr)
{
	ft_lstdel(&((*arr)->values), &free_array_elements);
}

void		free_array_elements(void *element, size_t sz)
{
	free_value((t_value**)(&element));
	sz = (size_t)sz;
}
