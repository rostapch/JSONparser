/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_booster.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 19:13:10 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 19:13:11 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		write_int(FILE *f, void *value)
{
	if (value)
		fprintf(f, "%d", ((int*)(value))[0]);
}

void		write_float(FILE *f, void *value)
{
	if (value)
		fprintf(f, "%f", ((float*)(value))[0]);
}

void		write_string(FILE *f, void *str)
{
	if (str)
	{
		fprintf(f, "\"");
		fprintf(f, "%s", ((t_string*)(str))->string);
		fprintf(f, "\"");
	}
}

void		write_pair(FILE *f, t_pair *pair, int tabs)
{
	int i;

	i = -1;
	while (++i < tabs)
		fprintf(f, "    ");
	write_string(f, pair->key);
	fprintf(f, ":");
	if (pair->value->value != NULL)
		value_write(f, *pair->value, tabs);
	else
		fprintf(f, "null");
}
