/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jsonparser.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/31 15:30:56 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/31 15:30:57 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_value		*parse_json_from_file(char *file)
{
	char	*json;
	char	*line;
	char	*temp;
	int		fd;
	t_value *ret;

	fd = open(file, O_RDONLY);
	if (fd == -1)
		exit_error(0);
	json = ft_strnew(0);
	while (get_next_line(fd, &line) > 0)
	{
		temp = ft_strjoin(json, line);
		ft_strdel(&json);
		ft_strdel(&line);
		json = temp;
	}
	close(fd);
	ret = parse_json(json);
	ft_strdel(&json);
	return (ret);
}

t_value		*parse_json(char *json)
{
	t_value	*input;

	if (!(json && (input = (t_value*)ft_memalloc(sizeof(t_value)))))
		return (NULL);
	input->type = OBJECT;
	input->parent = input;
	if (!(input->value = parse_object(&json, input)))
		ft_memdel((void **)&input);
	return (input);
}
