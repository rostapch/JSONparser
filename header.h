/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/31 15:30:42 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/31 15:30:44 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include "libft/includes/libft.h"

typedef struct s_string		t_string;
typedef struct s_array		t_array;
typedef struct s_value		t_value;
typedef struct s_object		t_object;
typedef struct s_pair		t_pair;

# define INT	0x01
# define FLOAT	0x02
# define STRING	0x04

# define OBJECT	0x08
# define ARRAY	0x10

# define BOOL	0x20
# define VALUE	0x40

struct			s_string
{
	size_t		length;
	char		*string;
};

struct			s_value
{
	int			type;
	t_value		*parent;
	void		*value;
};

struct			s_pair
{
	t_string	*key;
	t_value		*value;
};

struct			s_object
{
	int			amount;
	t_list		*pairs;
};

struct			s_array
{
	int			amount;
	t_list		*values;
};

/*
** parsing functions (constructors)
*/
t_value			*parse_json_from_file(char *file);
t_value			*parse_json(char *json);
t_value			*parse_value(char **json, t_value *prnt);
void			*parse_null(char **json);
void			*parse_digit(char **json, t_value *prnt);
float			*parse_float(char *str, t_value *prnt);
int				*parse_int(char *str, t_value *prnt);
t_string		*parse_string(char **json);
/*
** object
*/
t_object		*parse_object(char **json, t_value *prnt);
t_list			*pairs_list(char **json, t_value *prnt);
/*
** array
*/
t_array			*parse_array(char **json, t_value *prnt);
t_list			*values_list(char **json, t_value *prnt);

/*
** destructors
*/
void			free_value(t_value **val);
void			free_string(t_string **str);
/*
** object
*/
void			free_object(t_object **obj);
void			free_pairs(void *pair, size_t sz);
void			free_pair(t_pair **pair);
/*
** array
*/
void			free_array(t_array **arr);
void			free_array_elements(void *element, size_t sz);

/*
** search object's value by it's key
*/
t_value			*search_object(t_object obj, char *key);
void			*srch_val(t_value json, char *key);
void			*search_array(t_array arr, char *key);

/*
** write parsed json to file
*/
void			json_write(t_value json, char *file);
void			value_write(FILE *f, t_value json, int tabs);
void			write_int(FILE *f, void *value);
void			write_float(FILE *f, void *value);
void			write_string(FILE *f, void *str);
/*
** object
*/
void			write_obj(FILE *f, void *obj, int tabs);
void			write_pair(FILE *f, t_pair *pair, int tabs);
/*
** array
*/
void			write_array(FILE *f, void *arr, int tabs);

/*
** some special functions
*/
void			skip_spaces(char **json);
void			exit_error(int error);

#endif
