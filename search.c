/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:52:06 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:52:07 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		*search_array(t_array arr, char *key)
{
	t_list	*current;
	t_list	*next;

	current = arr.values;
	while (current)
	{
		next = current->next;
		return (srch_val(*(t_value*)(current->content), key));
		current = next;
	}
	return (NULL);
}

void		*srch_val(t_value json, char *key)
{
	if (json.type == ARRAY)
	{
		return (search_array(*(t_array*)(json.value), key));
	}
	else if (json.type == OBJECT)
	{
		return (search_object(*(t_object*)(json.value), key));
	}
	return (NULL);
}

t_value		*search_object(t_object obj, char *key)
{
	t_value *v;
	t_list	*current;
	t_list	*next;

	current = obj.pairs;
	v = NULL;
	while (current)
	{
		next = current->next;
		if (!ft_strcmp(((t_pair*)(current->content))->key->string, key))
		{
			return (((t_pair*)(current->content))->value);
		}
		if ((v = srch_val(*((t_pair*)(current->content))->value, key)) != NULL)
			return (v);
		current = next;
	}
	return (NULL);
}
