/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:55:27 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:55:31 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		*parse_null(char **json)
{
	char		*str;
	size_t		len;

	if (!(**json))
		return (NULL);
	len = 0;
	while (ft_isalpha((*json)[len]))
		len++;
	if (!len)
		return (NULL);
	if (!(str = ft_strsub(*json, 0, len)))
		return (NULL);
	(*json) += len;
	if (!ft_strcmp(str, "null") || !ft_strcmp(str, "NULL"))
	{
		ft_memdel((void **)&str);
		return (NULL);
	}
	return (NULL);
}
