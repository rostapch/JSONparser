/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   digits.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:49:57 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:49:59 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		*parse_digit(char **json, t_value *prnt)
{
	size_t	len;
	char	*str;

	len = 0;
	while ((*json)[len] && (ft_isdigit((*json)[len]) || (*json)[len] == '.'))
		len++;
	if (!(str = ft_strsub(*json, 0, len)))
		ft_memdel((void **)&str);
	(*json) += len;
	if (ft_strchr(str, '.'))
		return (parse_float(str, prnt));
	return (parse_int(str, prnt));
}

int			*parse_int(char *str, t_value *prnt)
{
	int		*ret;

	prnt->type = INT;
	if (!(ret = (int*)ft_memalloc(sizeof(int))))
		return (0);
	ret[0] = ft_atoi(str);
	ft_memdel((void**)&str);
	return (ret);
}

float		*parse_float(char *str, t_value *prnt)
{
	float		*ret;

	prnt->type = FLOAT;
	if (!(ret = (float*)ft_memalloc(sizeof(float))))
		return (0);
	ret[0] = ft_atof(str);
	ft_memdel((void**)&str);
	return (ret);
}
