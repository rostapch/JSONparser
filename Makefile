# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/09 17:48:26 by rostapch          #+#    #+#              #
#    Updated: 2017/10/09 17:48:28 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

NAME = libftjson

OBJFLAG = -c

HEADERS = -I . -I libft/includes

SRC_C =	jsonparser.c	somestaff.c		object.c	value.c\
		array.c			digits.c		string.c	search.c\
		special.c		write.c			write_booster.c

SRC_O = $(SRC_C:.c=.o)

all: $(NAME)

$(NAME): $(SRC_O)
	@ar rcv $(addsuffix .a, $(NAME)) $(SRC_O)

.c.o:
	@$(CC) $(CFLAGS) $(HEADERS) $(OBJFLAG) $< -o $@

clean:
	/bin/rm -f $(SRC_O)

fclean: clean
	/bin/rm -f $(addsuffix .a, $(NAME))

re: fclean all
.PHONY: clean all re fclean

#	gcc test.c -L libft/ -lft -L ./ -l ftjson -I .
