/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:42:21 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:42:22 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_object	*parse_object(char **json, t_value *prnt)
{
	t_object	*obj;

	prnt->type = OBJECT;
	if (!(**json) || (**json != '{'))
		return (NULL);
	(*json)++;
	skip_spaces(json);
	if (!(obj = (t_object*)ft_memalloc(sizeof(t_object))))
		return (NULL);
	ft_lstadd(&(obj->pairs), pairs_list(json, prnt));
	while (**json != '}')
	{
		skip_spaces(json);
		if (**json == ',')
		{
			(*json)++;
			ft_lstadd(&(obj->pairs), pairs_list(json, prnt));
		}
	}
	(*json)++;
	return (obj);
}

t_list		*pairs_list(char **json, t_value *prnt)
{
	t_pair		*pair;
	t_list		*pairs;

	if (!(pair = (t_pair*)ft_memalloc(sizeof(t_pair))))
		return (NULL);
	skip_spaces(json);
	pair->key = parse_string(json);
	skip_spaces(json);
	if (**json == ':')
		(*json)++;
	skip_spaces(json);
	pair->value = parse_value(json, prnt);
	pairs = ft_lstnew(pair, sizeof(t_pair));
	ft_memdel((void **)&pair);
	return (pairs);
}

void		free_object(t_object **obj)
{
	ft_lstdel(&((*obj)->pairs), &free_pairs);
}

void		free_pairs(void *pair, size_t sz)
{
	free_pair((t_pair**)(&pair));
	sz = (size_t)sz;
}

void		free_pair(t_pair **pair)
{
	free_string(&(*pair)->key);
	ft_memdel((void **)&(*pair)->key);
	(*pair)->key = NULL;
	free_value(&(*pair)->value);
	(*pair)->value = NULL;
	ft_memdel((void **)pair);
	*pair = NULL;
}
