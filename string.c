/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 18:49:45 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/09 18:49:47 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_string	*parse_string(char **json)
{
	t_string	*str;
	size_t		len;

	if (!(**json) || (**json != '\"'))
		return (NULL);
	(*json)++;
	len = 0;
	while ((*json)[len] && (*json)[len] != '\"')
		len++;
	if (!(len && (str = (t_string*)ft_memalloc(sizeof(t_string)))))
	{
		(*json)++;
		return (NULL);
	}
	str->length = len;
	if (!(str->string = ft_strsub(*json, 0, len)))
	{
		ft_memdel((void **)&str);
		return (NULL);
	}
	(*json) += len + 1;
	return (str);
}

void		free_string(t_string **str)
{
	ft_memdel((void **)&((*str)->string));
	(*str)->string = NULL;
}
